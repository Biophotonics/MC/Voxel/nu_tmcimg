= Monte-Carlo Simulation Software: '''tMCimg''' =

<html>
<img src="https://orbit.nmr.mgh.harvard.edu/wikidb/upload/tmcimg_sample.png" 
style="float:right" alt="Monte-Carlo Simulation" 
title="Light propagation in the human brain"/>
</html>

tMCimg uses a Monte-Carlo algorithm to model the transport of photons 
through 3D highly volumes with spatially varying optical properties and 
arbitrary boundary conditions. Both highly-scattering tissues (e.g. 
white matter) and weakly scattering tissues (e.g. cerebral spinal fluid) 
are supported. Using the clinical structural information provided by MRI, 
X-ray CT, or ultrasound, accurate solutions to the photon migration 
forward problem are found in times ranging from minutes to hours, 
depending on the optical properties and the computing resources available.

<toc>

== #  Introduction ==

This  section  describes  the  purpose  of  the  tMCimg application, 
and the basic algorithm/method implemented in the software.

=== # Purpose ===

Light transport in highly-scattering medium, such as human tissue, is a 
complex process. The photons go through a large number of scattering and 
absorption events when traveling through the volume. Monte-Carlo is a 
statistical approach which models the light transport process in such 
scenario.

With  this  software  package,  we  provide an efficient implementation 
of the Monte-Carlo method  for  modeling  the  light  transport  process  
in  arbitrarily-shaped  3D target (represented by voxelized space), for
both high or low-scattering medium. This program is not only able ot 
compute the static distributions of photons in complex media, but also 
capable of modeling  the  temporal  propagation of light in such media, 
which is critically important for data analysis in time-domain optical 
imaging systems.

=== # Method and References ===

The  details  of  our  implementation  and  validation  of  this  code 
can be found in the following  paper [Boas2002, see Reference Section].  
If  you  use  this  code in your research, the authors of the paper are 
appreciated if you can add the citation in your related publications.

== # Where to download and how to install ==

=== Download tMCimg ===

The source code of tMCimg is currently maintained at our ORBIT gforge
website under the project name "Brain Dynamo". The URL of the project 

https://orbit.nmr.mgh.harvard.edu/projects/braindynamo/

You can download the released versions of this software can be found at

https://orbit.nmr.mgh.harvard.edu/frs/?group_id=29

The latest development version are accessible via
CVS (concurrent version control) system at

https://orbit.nmr.mgh.harvard.edu/scm/?group_id=29

==== Prepare for CVS download ====
If you want to get the latest development branch of tMCimg, you can download it from our 
cvs. First, make sure you have installed cvs client on your system. If you are using 
Debian-based GNU/Linux systems (such as Ubuntu, Knoppix etc), you can install cvs by 
 sudo apt-get install cvs
if you are using Redhat-based GNU/Linux systems (such as Fedora, CentOS etc), you can do 
this by
 sudo yum install cvs
If your operation system is Windows, we recommend you to install 
[http://www.tortoisecvs.org/download.shtml TortoiseCVS].

==== Anonymous CVS Download ====

This project's CVS repository can be checked out through anonymous (pserver) CVS 
with the following instruction set. The module you wish to check out must be specified 
as the modulename. When prompted for a password for anonymous, simply press the 
Enter key.

 cvs -d :pserver:anonymous@orbit.nmr.mgh.harvard.edu:/cvsroot/braindynamo login
 cvs -d :pserver:anonymous@orbit.nmr.mgh.harvard.edu:/cvsroot/braindynamo checkout montecarlo

If you are using Windows, please check out [http://www.tortoisecvs.org/ TortoiseCVS website] for
the tutorial how to download code using the GUI (basically, right click on the file explorer,
and select menu CVS\CVS checkout...). 

==== Developer CVS Access via SSH ====

Only project developers can access the CVS tree via this method. SSH must be installed 
on your client machine. Substitute modulename and developername with the proper values. 
Enter your site password when prompted.

 export CVS_RSH=ssh
 cvs -d :ext:developername@orbit.nmr.mgh.harvard.edu:/cvsroot/braindynamo checkout montecarlo

=== Installing tMCimg ===

This  section  details the instructions to install the software on various commonly used
operating systems. Because this program only uses the standard ANSI C implementations, the
compilation  of  the  code  is relatively straightforward on various operating systems. It
normally  requires  a  minimum  installation  of a standard C/C++ compiler and the related
libraries.

==== Windows ====

We provided the project files for VC8.0 or above (tMCimg.vcproj) and 
for Borland Developer Studio 2006 (tMCimg_bcb.bdsproj) under the src/
directory. You can simply open these project files and select "build".
To get the maximum performance, you can find the "Configuration manager"
and choose "Release" mode. Both compilers are available for free from
they respective website.

Although it is not recommended, you can also use command line
to compile the software. Here we assume you are using the free
Borland C++ Compiler 5.5.

The following instructions have been tested for Borland C++ Compiler v.5.5.1 on Windows XP:

1. In a DOS shell, check the values of the environment variables CC_ROOT_PATH, CC_INCLUDE_PATH, 
and CC_LIB_PATH by typing 

   echo %CC_ROOT_PATH%
   echo %CC_INCLUDE_PATH%
   echo %CC_LIB_PATH%

If the paths in these variable do not match the paths of your compiler then 
open the install_win.bat file in a text editor and set them to reflect the  
installation paths of your Borland C++ compiler. 

2. Then run install_win.bat by typing 

   call install_win.bat in 

at the DOS prompt. This should create the makefiles specific to the Windows and 
Borland C++ Compiler environment. 

3. Now you're ready to compile: from the root montecarlo directory type make, 
make opt (for best performance) or make debug (to be able to the load and run 
in a debugger) 

====   Linux and Mac OS ====

To  compile  the code from source, you need to have gcc installed in your system. This can
be done by sudo apt-get install build-essential in Debian based systems (such as Ubuntu), or
sudo yum install gcc in Fedora/Redhat based distributions.

To compile tMCimg first create the makefiles for the Linux/Mac environment by running the 
install_linux_mac.sh shell script from the root montecarlo directory. Then type 

  make

or for better performance

  make opt

or

  make debug 

so that it can be loaded and run in a debugger.

== # Detailed Usage Instructions ==

=== # Directory structure ===

After downloading the montecarlo module you should have a directory structure like 
this:

                 montecarlo/
                     |
                     |
  bin/  CVS/  example/  makefile  obj/  src/


=== # Building the tMCimg executable ===

Go to the root directory under ./montecarlo and type make on the command line. 
This should create the tMCimg executable under ./bin. 
 
  cd <path name to root>/montecarlo
  make


=== # Running tMCimg ===

To run tMCimg you have to have two files: a config file with a .inp extension 
and a segmentation file. tMCimg takes one argument, the name of the .inp file 
which has a reference to the segmentation file. See 4 for explanation of how 
to create a config file

=== # Config (.inp) file format for M tissue types, N detectors , line-by-line ===

 line 1:              number of photons                                                    
 line 2:              random number seed used to generate scattering length and 
                      angle for each photon's path.
 line 3:              3D coordinates of the source (in mm)
 line 4:              vector showing initial direction at which photons are injected 
                      into tissue from the source
 line 5:              time gates specifying the time periods which to record the 
                      photon path.
                      1st column is the start time,  2nd column is the end time
                      3rd column is the time step size (in s). 
 line 6:              pathname of tissue segmentation file  
 lines 7 - 9:         1st column specifies voxel size (in mm) in the x, y, and z directions
                      2nd column specifies volume size in voxels (not mm) in the x,y,and z 
                      directions
                      3rd and 4th columns specify the subsection of interest in 
                      the entire volume which to limit recording the fluence to. The 3rd 
                      column is the index of the starting voxel and the 4th the ending. 
 line 10:             number of tissue types
 lines 11 to 11+M-1:  properties of M tissues: 
                      column 1 - scattering (mus), column 2 - anisotropy (g),
                      column 3 - absorption (mua), column 4 - refraction (n)
 line 11+M:           Specifies the number of detectors and radius of detector (in mm)
 lines 11+M+1 to
       11+M+N:        3D coordinates of N detector (in mm)  

Example: 

In matlab, create a 100x100x100 volume containing a 100x100x99 
homogeneous slab:

    slab = zeros(100,100,100);
    slab(:,:,2:end)=1;
    fid = fopen('slab100x60x80.bin', 'wb');
    fwrite(fid, slab, 'uint8');
    fclose(fid);

==== # Example of a .inp file with line-by-line explanatory comments. ====

 100000                   -- Launch 100000 photons
 -4896744                 -- random number seed -4896744 used to generate 
                             scattering length and angle for every photon.
 50 50 2                  -- source coordinates (50mm,50mm,2mm) - the initial position 
                             of every photon.                            
 0 0 1                    -- vector showing initial direction of photons as  
                             at which photons are injected into tissue from 
                             the source position
 0 5e-09 5e-09            -- time gates: recording of fluence for each photon starts at time 
                             0 and ends at 5 ns. Since time step is also 5 ns there is only one
                             time gate.
 ./slab100x100x100.bin    -- the segmentation volume is a 100x100x99 slab contained in a 
                             a 100x100x100 volume (1 layer is all zeros). It is 
                             loaded from a file named ./slab100x100x100.bin.
 1 100 1 100              -- the 1st column specifies a 1x1x1mm voxel size. The 2nd
 1 100 1 100                 column specifies a 100x100x100 volume in voxels (not mm). 
 1 100 1 100                 The 3rd and 4th columns specify that the subsection in which 
                             to record photon fluence is the entire volume.
 1                        -- The volume is homogeneous (1 tissue type).
 0.66 0.01 0.02 1         -- Tissue properties os the 1 tissue are: mus=0.66, g=0.01, mua=0.02, 
                             refraction index=1
 1 1                      -- There's one detector, with a radius of 1mm.
 60 50 2                  -- the detector is at position (60mm,50mm,2mm)




== # Examples of using tMCimg  ==
              
=== # Running tMCimg on the included sample data ===

The montecarlo module available on orbit includes sample data for 
running tMCimg; that is segmentation and config files. They are included 
with the under ./example/sphere. 

To run tMCimg on the example data, simply do the following from the 
root montecarlo directory (on Linux or Mac OS):

 cd ./example/sphere
 ./runmc40.sh
 ./runmc80.sh

This will create data for the 2 separate examples; one for the 40x40x40 segmented 
sphere with 2 tissue types, 1 source and 1 detector and one for the 80x80x80 segmented 
sphere with 2 tissue types, 1 source and 1 detector. After running the scripts (which run 
tMCimg) you can examine the outputs (the .2pt and .his files) in Matlab. 


=== # Creating all input data from scratch, then running tMCimg ===

In this section, we demonstrate how to prepare and run a simulation
session using Matlab and tMCimg. We will generate a 100x100x100 
volume with 4 tissue types, and place 5 optodes on the surface 
and run tMCimg with 5 million photons on each optode.


a) Generate a 100x100x100 segmentation volume with 4 tissue types. Name it seg.bin

  seg = zeros(100,100,100);
  seg(:,:,10:25) = 1;
  seg(:,:,26:50) = 2;
  seg(:,:,51:75) = 3;
  seg(:,:,76:end) = 4;
  fid = fopen('seg.bin', 'wb');
  fwrite(fid, seg, 'uint8');
  fclose(fid);

b) Position the source and 4 detectors at the following locations

  s1: (50, 30, 10)
  d1: (50, 35, 10)
  d2: (50, 25, 10)
  d3: (55, 30, 10)
  d4: (45, 30, 10)

generating 5 million photons and one time gate. 

Create the .inp config files for all optodes for which you want to 
create a 2pt fluence file. For instance, for source 1, you can 
create a file named sample.s1.inp. (The file name is arbitrary for 
tMCimg as long as it has a .inp extension. But it's a good idea to 
include the optode type and number to know which optode the config 
file uses as the source.) 

Here's what the contents of two of the 5 input files - sample.s1.inp 
and sample.d1.inp - might look like:

sample.s1.inp: 

  5000000
  -75126705
  50.0 30.0 10.0
  0.0 0.0 1.0
  0 5.000000e-09 5.000000e-09
  ./seg.bin
  1 100 1 100
  1 100 1 100
  1 100 1 100
  4
  0.660000 0.001000 0.019100 1.000000
  0.860000 0.001000 0.013600 1.000000
  0.010000 0.001000 0.002600 1.000000
  1.100000 0.001000 0.018600 1.000000
  4 1
  50.0        35.0    10.0
  50.0        25.0    10.0
  55.0        30.0    10.0
  45.0        30.0    10.0
    

sample.d1.inp: 

  5000000
  -25509511
  50.0 35.0 10.0
  0.0 0.0 1.0
  0 5.000000e-09 5.000000e-09
  ./seg.bin
  1 100 1 100
  1 100 1 100
  1 100 1 100
  4
  0.660000 0.001000 0.019100 1.000000
  0.860000 0.001000 0.013600 1.000000
  0.010000 0.001000 0.002600 1.000000
  1.100000 0.001000 0.018600 1.000000
  4 1
  50.0        30.0    10.0
  50.0        25.0    10.0
  55.0        30.0    10.0
  45.0        30.0    10.0

Then on the command line in whichever shell you're running, run tMCimg for 
each optode you need. In this example there are 5 optodes, so to get fluence 
data for all the optodes run tMCimg 5 times, without including the .inp 
extension, like this:

 tMCimg sample.s1
 tMCimg sample.d1
 tMCimg sample.d2
 tMCimg sample.d3
 tMCimg sample.d4
    
This will generate the .2pt and .his files for all 5 optodes. 

== #  Bug Report and Discussion Forum ==

The bug reporting and discussion forum are still works in progress, soon to be revisited. 


== # Collaboration and Development ==

The  project  is  licensed  under  a  BSD license (please read the details in the
LICENSE).  This means that users can not only use the program, but also can be involved in
the  collaborative  development  of  the program. The maintainer of this software are very
excited to see more developers contribute to this software.

The     latest     code    can    be    accessed    from    our    CVS    repository    at
https://orbit.nmr.mgh.harvard.edu/scm/?group_id=29    where   the   module   name   is
"montecarlo".     One     can    also    browse    the    latest    source    code    from
https://orbit.nmr.mgh.harvard.edu/plugins/scmcvs/cvsweb.php/montecarlo/?cvsroot=braindynamo

Users  can download the program without creating a gforge account. However, if one wants to
be involved in the development, one can consider the following:

* if  your  change  is relatively small, submit your changes as a patch file, send it to \
our  current  maintainer  and they will review the patch and add it to the main source \
code tree
* or  if  you  want  to  be  involved in the maintaining, forking, adding new modules or \
significantly  rewrite  the  code,  please  discuss it with our maintainer and we will \
consider  to  add  you to the developer list so that you can commit changes to the cvs \
repository.


== # References ==

* Boas  DA,  Culver  JP,  Stott JJ, and Dunn AK. "Three dimensional Monte Carlo code for \
photon  migration  through complex heterogeneous media including the adult human head" \
Opt. Express 10: 159-170 (2002).


