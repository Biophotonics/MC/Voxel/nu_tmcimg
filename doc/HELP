

(1) After downloading the montecarlo module you should have a directory 
    structure like this:

                         montecarlo

                             |
                             |

          bin/  CVS/  example/  makefile  obj/  src/


(2) Building tMCimg executable:

    Go to the root directory under ./montecarlo and type make 
    on the command line. This should create the tMCimg executable 
    under ./bin. 
 
    $ cd <path name to root>/montecarlo
    $ make


(3) Running tMCimg. 
    To run tMCimg you have to have two file: a config file with a .inp extension 
    and a segmentation file. tMCimg takes one argument, the .inp file which has 
    a reference to the segmentation file. See 4 for explanation of how 
    to create a config file

    Example: 

    We have the config file babyXX2.inp and a segmentation file baby_XXwks.bin. The
    reference to baby_XXwks.bin is in babyXX2.inp. To run you can do the following

    $ cd ./montecarlo
    $ ./bin/tMCimg babyXX2

  
   
(4) Config file format line-by-line:

    line 1:        number of photons
    line 2:        seed for generating random path for each photon
    line 3:        3D coordinates of the source 
    line 4:        vector showing initial direction of photons as they enter the 
                   medium from the source. 
    line 5:        temporal gate: start time; stop time; step size. Note that if 
                   step size is bigger than start minus stop time, then tMCimg 
                   will record only one time period. 
    line 6:        pathname of tissue segmentation file  
    line 7:        voxel size along x zxis; volume size in voxels along x axis;  
                   last 2 numbers: region of interest start/end along x axis, the 
                   photon propagation and fluence recording will be limited to that 
                   region
    line 8:        voxel size along y; volume size in voxels along y;  
                   last 2 numbers: region of interest start/end along y axis
    line 9:        voxel size along z; volume size in voxels along z;  
                   last 2 numbers: region of interest start/end along z axis
    line 10:       m - number of tissue types
    line 11:       properties of tissue 1: scattering (mus); anisotropy (g); 
                   absorption (mua); refraction (n)
                          . . . . . . . 
    line 11+m:     properties of tissue m: scattering (mus); anisotropy (g); 
                   absorption (mua); refraction (n)
    line 11+m+1:   n - number of detectors; radius of detector
    line 11+m+2:   3D coordinates of detector 1
                          . . . . . . . 
    line 11+m+2+n: 3D coordinates of detector n

    
    ------------
    Example of .inp file (babyXX2.inp)
    
    10000000               
    -4896744               
    242   110   107        
    -1 0 0                 
    0 5e-9 6e-9            
    baby_XXwks.bin         
    1 308   1 308          
    1 308   1 308          
    1 308   1 308          
    4                      
    0.75   0.01   0.009  1 
    0.5    0.01   0.002  1 
    0.5    0.01   0.005  1 
    0.5    0.01   0.005  1 
    11 2                   
    245   110   116        
    247   110   126        
    247   110   136        
    247   110   146        
    246   110   156        
    243   110   166        
    241   110   176        
    238   110   186        
    235   110   195        
    230   110   203        
    225   110   212        

   
