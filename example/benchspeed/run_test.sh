#!/bin/sh
if [ ! -e semi62x62x62.bin ]; then
  dd if=/dev/zero of=semi62x62x62.bin bs=3844 count=62
  perl -pi -e 's/\x0/\x1/g' semi62x62x62.bin
fi

time ../../bin/tMCimg qtest
